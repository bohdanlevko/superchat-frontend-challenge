const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const config = require("config");
const app = express();
const port = config.get("port");
const mongoUrl = config.get("mongoUrl");

async function startDbConnection() {
  await mongoose.connect(mongoUrl);
}
app.use(cors());
app.use(express.json());
app.use("/api/repoLink", require("/repoLink/repoLink.routes"));
app.use("/uploads", express.static("uploads"));

async function start() {
  try {
    await startDbConnection();
    app.listen(port, () => console.log(`App has been started on ${port}...`));
  } catch (e) {
    console.log("Server Error", e.message);
    process.exit(1);
  }
}

start();
